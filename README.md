一些小实验<br>

|语言| 文件夹 | 实验 | 
|----|-----| ----: | 
|Java|[Runoob](./Runoob) |java读写mysql | 
|Java|[window_learn](./window_learn)| sliding-count-window | 
|Java|[wordcount](./wordcount)|wordcount
|Scala|[scala_helloworld](./scala_helloworld)|Spring helloworld



实验总目录:

|实验|备注|
|---|---|
|[Flink CEP实验](flink_cep/README.md)|网络收集|
|[dataset_api](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/代码文件说明.md)|flink官方文档中所有dataset的api完整用例(leftOuterJoin和rightOuterJoin效果有瑕疵，钉钉群的官方技术支持也不懂)
|[DataStream_api](./datastream_api/Java/代码文件说明.md)|flink官方文档中所有datastream的api完整用例
|[水位线实验汇总](./水位线实验/README.md)|网络收集(完成)|
|[Flink的state实验](./flink_state/README.md)|尚未全部完成
|[Flink SQL Cookbook](flink-sql-cookbook/README.md)|完成
|[Table_api](table_api/Java/README.md)|完成(1.12向官方汇报一个bug,需要1.13才能解决)
|[Flink的各种读写实验](FLINK读写各种数据源/README.md)|正在进行中
|[Flink数据倾斜](Flink数据倾斜/README.md)|沒有完成
|[Flink读Kafka](./flink读kafka/README.md)|
|[Hive动态分区/静态分区用法](hive动态静态分区)|完成
|[UDF写法(官方文档)](UDF写法研究/README.md)|完成
|[flink清洗数据案例-IOUtis用法](flink清洗数据案例/IOUtis/README.md)|完成
|[flink清洗数据案例-FastJson用法](flink清洗数据案例/FastJson_Learn/README.md)|完成
|[HIVE_UDF](HIVE_UDF/README.md)|完成
|[Flink时区问题](Flink时区问题)|完成





