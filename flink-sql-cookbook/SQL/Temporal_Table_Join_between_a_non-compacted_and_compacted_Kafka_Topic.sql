

---當交易發生時，交易表與匯率表做join計算

## Script
CREATE TEMPORARY TABLE currency_rates (
  `currency_code` STRING,
  `eur_rate` DECIMAL(6,4),
  `rate_time` TIMESTAMP(3),
  WATERMARK FOR `rate_time` AS rate_time - INTERVAL '15' SECONDS,
  PRIMARY KEY (currency_code) NOT ENFORCED
) WITH (
  'connector' = 'upsert-kafka',
  'topic' = 'currency_rates',
  'properties.bootstrap.servers' = 'Desktop:9091',
  'key.format' = 'raw',
  'value.format' = 'json'
);

CREATE TEMPORARY TABLE transactions (
  `id` STRING,
  `currency_code` STRING,
  `total` DECIMAL(10,2),
  `transaction_time` TIMESTAMP(3),
  WATERMARK FOR `transaction_time` AS transaction_time - INTERVAL '30' SECONDS
) WITH (
  'connector' = 'kafka',
  'topic' = 'transactions',
  'properties.bootstrap.servers' = 'Desktop:9091',
  'key.format' = 'raw',
  'key.fields' = 'id',
  'value.format' = 'json',
  'value.fields-include' = 'ALL'
);


---

CREATE TEMPORARY TABLE currency_rates_faker
WITH (
  'connector' = 'faker',
  'fields.currency_code.expression' = '#{Currency.code}',
  'fields.eur_rate.expression' = '#{Number.randomDouble ''4'',''0'',''10''}',
  'fields.rate_time.expression' = '#{date.past ''15'',''SECONDS''}', 
  'rows-per-second' = '100'
) LIKE currency_rates (EXCLUDING OPTIONS);

INSERT INTO currency_rates SELECT * FROM currency_rates_faker;

#### Kafka Topic

$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic currency_rates

````
{"currency_code":"HUF","eur_rate":5.3964,"rate_time":"2021-02-07 11:17:52"}
{"currency_code":"TND","eur_rate":9.1991,"rate_time":"2021-02-07 11:17:51"}
{"currency_code":"MNT","eur_rate":8.722,"rate_time":"2021-02-07 11:17:46"}
{"currency_code":"PGK","eur_rate":8.4363,"rate_time":"2021-02-07 11:17:50"}
{"currency_code":"SVC","eur_rate":7.0003,"rate_time":"2021-02-07 11:17:42"}
{"currency_code":"EGP","eur_rate":3.1763,"rate_time":"2021-02-07 11:17:49"}
{"currency_code":"TRY","eur_rate":9.442,"rate_time":"2021-02-07 11:17:47"}
{"currency_code":"ZMK","eur_rate":5.4028,"rate_time":"2021-02-07 11:17:48"}
{"currency_code":"KHR","eur_rate":2.2608,"rate_time":"2021-02-07 11:17:44"}
{"currency_code":"TWD","eur_rate":0.8893,"rate_time":"2021-02-07 11:17:51"}
{"currency_code":"AUD","eur_rate":1.8412,"rate_time":"2021-02-07 11:17:46"}
{"currency_code":"OMR","eur_rate":6.0807,"rate_time":"2021-02-07 11:17:43"}
{"currency_code":"MRO","eur_rate":4.45,"rate_time":"2021-02-07 11:17:41"}
{"currency_code":"GTQ","eur_rate":4.886,"rate_time":"2021-02-07 11:17:42"}
{"currency_code":"THB","eur_rate":8.5364,"rate_time":"2021-02-07 11:17:39"}
````


#### Script

```sql
CREATE TEMPORARY TABLE transactions_faker
WITH (
  'connector' = 'faker',
  'fields.id.expression' = '#{Internet.UUID}',
  'fields.currency_code.expression' = '#{Currency.code}',
  'fields.total.expression' = '#{Number.randomDouble ''2'',''10'',''1000''}',
  'fields.transaction_time.expression' = '#{date.past ''30'',''SECONDS''}',
  'rows-per-second' = '100'
) LIKE transactions (EXCLUDING OPTIONS);

INSERT INTO transactions SELECT * FROM transactions_faker;
```

#### Kafka Topic

$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic currency_rates transactions



```
{"id":"cd643480-4e2a-4568-8c4f-3538531917a5","currency_code":"ETB","total":789.1,"transaction_time":"2021-02-07 11:31:57"}
{"id":"728d0a89-961d-4d7d-8dd2-3a9411626c67","currency_code":"AZN","total":447.94,"transaction_time":"2021-02-07 11:32:05"}
{"id":"dc9e2101-6bdb-4ad5-8b38-b238f825a8df","currency_code":"GEL","total":145.16,"transaction_time":"2021-02-07 11:32:14"}
{"id":"6c596265-fe07-4d27-b0d2-16d0437033e8","currency_code":"AWG","total":935.53,"transaction_time":"2021-02-07 11:32:20"}
{"id":"740f1323-4be7-49a4-bd44-cf25d281260f","currency_code":"LYD","total":175.77,"transaction_time":"2021-02-07 11:31:57"}
{"id":"cace8a7a-f87f-4b99-a522-8de70a804eab","currency_code":"UZS","total":589.13,"transaction_time":"2021-02-07 11:32:11"}
{"id":"f7fb48a9-1224-4dc0-8bd6-34566fc9efe6","currency_code":"MZN","total":20.08,"transaction_time":"2021-02-07 11:31:57"}
{"id":"64b51e0c-addb-436c-b1d0-3c00bc47ad05","currency_code":"BHD","total":839.33,"transaction_time":"2021-02-07 11:32:09"}
{"id":"09e53825-d230-49ff-84b8-4f9c378ced30","currency_code":"GBP","total":542.81,"transaction_time":"2021-02-07 11:31:57"}
{"id":"821b7d7c-0df1-4b96-b548-5dfe8979ec31","currency_code":"MZN","total":321.68,"transaction_time":"2021-02-07 11:32:09"}
{"id":"6f7c0a7a-977a-4b51-a98b-ceee53a7e7b2","currency_code":"XPD","total":45.67,"transaction_time":"2021-02-07 11:32:17"}
{"id":"adc1d6e8-60a3-46bd-b474-a535558b5b43","currency_code":"VEF","total":428.73,"transaction_time":"2021-02-07 11:31:53"}
{"id":"db3152b0-901a-48fb-a3f4-1914f2222f5e","currency_code":"MAD","total":787.21,"transaction_time":"2021-02-07 11:32:03"}
{"id":"1d8438cd-e9e1-4143-885e-c4298b5b82b6","currency_code":"PEN","total":710.68,"transaction_time":"2021-02-07 11:32:11"}

```


---进行join

SELECT 
  t.id,
  t.total * c.eur_rate AS total_eur,
  t.total, 
  c.currency_code,
  t.transaction_time
FROM transactions t
JOIN currency_rates FOR SYSTEM_TIME AS OF t.transaction_time AS c
ON t.currency_code = c.currency_code;
