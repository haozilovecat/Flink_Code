
CREATE TABLE server_logs ( 
    client_ip STRING,
    client_identity STRING, 
    userid STRING, 
    request_line STRING, 
    status_code STRING, 
    log_time AS PROCTIME()
) WITH (
  'connector' = 'faker', 
  'fields.client_ip.expression' = '#{Internet.publicIpV4Address}',
  'fields.client_identity.expression' =  '-',
  'fields.request_line.expression' =  '-',
  'fields.userid.expression' =  '-',
  'fields.log_time.expression' =  '#{date.past ''15'',''5'',''SECONDS''}',
  'fields.status_code.expression' = '#{regexify (200|201|204|400|401|403|301){1}''}'
);


---最終結果是兩列:
---①獨立IP
---②採樣時刻點(窗口滑動10s採樣一次)
---
---該SQL目的是:
---每隔一段時間(10秒),統計這段時間內有多少獨立ip訪問


SELECT  
  COUNT(DISTINCT client_ip) AS ip_addresses,
  TUMBLE_PROCTIME(log_time, INTERVAL '10' second) AS window_interval
FROM server_logs
GROUP BY 
  TUMBLE(log_time, INTERVAL '10' second);