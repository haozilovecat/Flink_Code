# 21 Lateral Table Join


CREATE TABLE People (
    id           INT,
    city         STRING,
    state        STRING,
    arrival_time TIMESTAMP(3),
    WATERMARK FOR arrival_time AS arrival_time - INTERVAL '1' MINUTE 
) WITH (
    'connector' = 'faker',
    'fields.id.expression'    = '#{number.numberBetween ''1'',''100''}',
    'fields.city.expression'  = '#{regexify ''(Newmouth|Newburgh|Portport|Southfort|Springfield){1}''}',
    'fields.state.expression' = '#{regexify ''(New York|Illinois|California|Washington){1}''}',
    'fields.arrival_time.expression' = '#{date.past ''15'',''SECONDS''}',
    'rows-per-second'          = '10'
); 



-- city、state、population三个列
---row_number() over的意思就是对前面的数据进行排序(排序依据看order by)
-- 每个州的人进行先来后到的排序(row_numebr over 操作)
-- 然后统计 人数
CREATE TEMPORARY VIEW CurrentPopulation AS
SELECT 
    city,
    state,
    COUNT(*) as population
FROM (
    SELECT
        city,
        state,
        ROW_NUMBER() OVER (PARTITION BY id ORDER BY arrival_time DESC) AS rownum
    FROM People
)
WHERE rownum = 1
GROUP BY city, state;


-- 遍历每个州，然后遍历大衣呢该州的各个城市和各城市对应的人口(把上面一个表格的两个字段的顺序换了下)
-- lateral join用来2重循环(2个表格)遍历。
-- 只统计每个州人口最大的两个城市
SELECT
    state,
    city,
    population
FROM 
    (SELECT DISTINCT state FROM CurrentPopulation) States,
---这里的States是去重后的结果
    LATERAL (
        SELECT city, population
        FROM CurrentPopulation
        WHERE state = States.state---外循环结果
        ORDER BY population DESC ---对原来的人口表格CurrentPopulation进行人口排序，只留人口最大的2个城市
        ---内循环结果
        LIMIT 2
);
-- 最终得到的是state/city/population三列
