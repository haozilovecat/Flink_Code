import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._

object WindowLife {
        def main(args: Array[String]): Unit = {
        val env = StreamExecutionEnvironment.getExecutionEnvironment

        val wordDS = env.socketTextStream("Desktop",3456)

        wordDS
        .map((_,1))
        .keyBy(0)
        //累计单个Key中3条数据就进行处理
        .countWindow(3)
        .sum(1)
        .print("测试：")


//                每输入三个相同的字符串进行求和操作
        env.execute()
        }
        }