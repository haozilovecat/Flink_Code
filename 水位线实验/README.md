水位线实验汇总<br>

|实验|运行办法|
|---|---|
|[窗口的生命周期](./src/main/scala/WindowLife.scala)|①nc -lk 3456<br>②启动程序<br>③输入字符串<br>yuchi<br>yuchi<br>yuchi|
|[滚动窗口](./src/main/scala/TumblingWindow.scala)|使用的是系统处理时间,<br>而非Event Time<br>也就是说并非nc -lk中输入的时间戳|
|[滑动窗口](./src/main/scala/TimeWindow.scala)|一条数据出现在多个窗口
|[会话窗口](./src/main/scala/ProcessingTime.scala)|每间隔10s输入收到的数据
|[WaterMark原理](./src/main/java/StreamingWindowWaterMark.java)|输入0001,1538359882000<br>0001,1538359886000<br>0001,1538359892000<br>0001,1538359893000<br>0001,1538359894000<br>0001,1538359896000<br>0001,1538359897000<br>0001,1538359899000<br>0001,1538359891000(乱序数据)<br>0001,1538359903000(乱序数据)<br>0001,1538359890000(延迟数据)<br>0001,1538359903000(延迟数据)<br>0001,1538359891000(延迟数据)<br>0001,1538359892000(延迟数据)
|[收集迟到的数据](./src/main/java/StreamingWindowWatermark2.java)|输入<br>0001,1538359890000<br>0001,1538359903000<br>0001,1538359890000<br>0001,1538359891000<br>0001,1538359892000|
|[一篇参考文献中的水位线实验](./src/main/java/WaterMark_disorder.java)|[参考文献](http://www.louisvv.com/archives/2225.html)
