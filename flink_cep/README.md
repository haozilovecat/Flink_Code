


Flink CEP實驗匯總(滿足一定規則的流數據的檢測)<br>

|语言|  实验<br>(滿足一定規則的流數據的檢測) |
|---|---| 
|Scala|[flink的cep入門實驗](./src/main/scala/Flink_cep_csdn.scala)|
|Scala|[订单超时检测](./src/main/scala/OrderTimeout.scala)
|Java|[Flink - CEP分析攻击行为](./src/main/java/FlinkLoginFail.java)|
|Java|[異常登錄檢測](./src/main/java/CepEvent.java)|
