import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

public class WindowFunction2 implements WindowFunction<Tuple2<String, Long>, Tuple2<String, Long>, Tuple, TimeWindow>
{
@Override
public void apply(Tuple tuple, TimeWindow window, Iterable<Tuple2<String, Long>> input, Collector<Tuple2<String, Long>> out) throws Exception {
        long sum = 0L;
        int count = 0;
        System.out.printf("count=%d",count);
        System.out.println("----------------------------1----------------------------------------------------");
        for (Tuple2<String, Long> record: input)
        {
        sum += record.f1;
        count++;
        }
        Tuple2<String, Long> result = input.iterator().next();
        result.f1 = sum / count;
        out.collect(result);
        }
}


