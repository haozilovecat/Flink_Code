import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.{RequestMapping, RestController}
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter


@SpringBootApplication(scanBasePackages = Array("scala"))
@RestController
@RequestMapping(Array("/hello"))
class AppConf extends WebMvcConfigurerAdapter{

  @RequestMapping(Array("","/"))
  def index():String = {
    return "Hello World scala Spring boot":String
  }

}

