---Flink SQL Client中建立source
    CREATE TABLE source (
     name string,
     age bigint,
     sex string,
     grade bigint,
     rate float
    ) WITH (
      'connector.type' = 'filesystem',                
      'connector.path' = '/home/appleyuchi/桌面/写入ClickHouse/source.csv', 
      'format.type' = 'csv',                         
      'format.field-delimiter' = ',' 
    );
select * from source;

---读数时间大概是11s
---service firewalld stop
---Flink SQL Client中建立sink
CREATE TABLE sink_table (
    name VARCHAR,
    grade BIGINT,
    rate FLOAT,
    PRIMARY KEY (name) NOT ENFORCED
) WITH (
    'connector' = 'clickhouse',
    'url' = 'clickhouse://127.0.0.1:8123',
    'table-name' = 'd_sink_table',
    'sink.batch-size' = '1000',
    'sink.partition-strategy' = 'hash',
    'sink.partition-key' = 'name',
    'username' = 'default',
    'password' = 'appleyuchi'
);
INSERT INTO sink_table SELECT name, grade, rate FROM source;


---下面是在clickhouse客户端中执行的
create table d_sink_table
(
    name  String,
    grade Int64,
    rate  Float32,
    more  Float32
)
    engine = Memory;





