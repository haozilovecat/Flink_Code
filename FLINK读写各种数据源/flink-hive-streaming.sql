


$FLINK_HOME/bin/sql-client.sh embedded -d $FLINK_HOME/conf/flink-hive-streaming.yaml


----------------------------------
use catalog myhive;

SET table.sql-dialect=hive;

drop table if exists hive_table;

CREATE TABLE hive_table (
  f_sequence INT,
  f_random INT,
  f_random_str STRING
) PARTITIONED BY (dt STRING, hr STRING, mi STRING) STORED AS parquet TBLPROPERTIES (
  'partition.time-extractor.timestamp-pattern'='$dt $hr:$mi:00',
  'sink.partition-commit.trigger'='partition-time',
  'sink.partition-commit.delay'='1 min',
  'sink.partition-commit.policy.kind'='metastore,success-file'
);



SET table.sql-dialect=default;
drop table if exists datagen;
CREATE TABLE datagen (
 f_sequence INT,
 f_random INT,
 f_random_str STRING,
 ts AS localtimestamp,
 WATERMARK FOR ts AS ts
) WITH (
 'connector' = 'datagen',
 -- optional options --
 'rows-per-second'='5',

 'fields.f_sequence.kind'='sequence',
 'fields.f_sequence.start'='1',
 'fields.f_sequence.end'='50',-- 这个地方限制了一共会产生的条数

 'fields.f_random.min'='1',
 'fields.f_random.max'='50',

 'fields.f_random_str.length'='10'
);



insert into hive_table select f_sequence,f_random,f_random_str ,DATE_FORMAT(ts, 'yyyy-MM-dd'), DATE_FORMAT(ts, 'HH') ,DATE_FORMAT(ts, 'mm') from datagen;
