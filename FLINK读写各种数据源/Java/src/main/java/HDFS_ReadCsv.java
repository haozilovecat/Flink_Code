import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

public class HDFS_ReadCsv {
    public static void main(String[] args) throws Exception
    {

    ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

    DataSet<OrderCost> originData=env.readCsvFile("hdfs://Desktop:9000/stats_order_total_xcost.csv").fieldDelimiter(",")
            .ignoreFirstLine()
            .includeFields(true,true,true,false,true)
            .pojoType(OrderCost.class,"parentOrderNo","memberId","unionId","createTime");
        originData.print();

}

}
