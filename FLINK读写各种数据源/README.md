|代码功能|备注(操作步骤、博客链接等)|
|---|---|
|Flink读写ClickHouse||
|[Flink写HDFS(Txt文件)](Java/src/main/java/HDFS_WriteTxt.java)|①启动hadoop<br>②离开安全模式<br>③导入依赖flink-shaded-hadoop-3-uber-3.1.1.7.0.3.0-79-7.0.jar<br>④运行HDFS_Read_Write<br>⑤hdfs dfs -cat /test001.txt|
|[Flink读HDFS(Txt文件)](Java/src/main/java/HDFS_ReadTxt.java)|①启动hadoop<br>②离开安全模式<br>③导入依赖flink-shaded-hadoop-3-uber-3.1.1.7.0.3.0-79-7.0.jar<br>④运行HDFS_ReadTxt<br>⑤hdfs dfs -cat /test004.txt
|[Flink读HDFS(Csv文件)](Java/src/main/java/HDFS_ReadCsv.java)|
|[Flink写HDFS(Csv文件](Java/src/main/java/HDFS_WriteCsv.csv)|Csv写入HDFS后是分片形式存储的,<br>并不是在HDFS上的一个Csv文件.
|[Flink读mysql-scala](Scala/src/main/scala/Flink_MysqlSource.scala)|完成|
|[Flink读mysql-java](Java/src/main/java/MysqlSource.java)|完成
|[Flink写mysql-java](Java/src/main/java/BatchDemoOperatorMysql.java)|完成
|Flink读Kafka(SQL Client+DDL方式)|[Flink SQL Client读Kafka+流计算](https://yuchi.blog.csdn.net/article/details/111651273)|
|[Flink读Kafka(DDL嵌入代碼方式)](./Java/src/main/java/KafkaFlinkDDL.java)|完成(與上面的實驗結果完全一致)|
|[Flink写Hbase](Kafka_Flink_Hbase.txt)|[Kafka-＞Flink-＞Hbase(纯DDL/SQL形式)](https://yuchi.blog.csdn.net/article/details/111756492)|
|[Flink读hive](Java/src/main/java/Hive_Read.java)|完成|
|[Flink datagen->Hive(Streaming)](flink-hive-streaming.sql)|[Flink SQL Client方言切换与datagen-＞Hive](https://yuchi.blog.csdn.net/article/details/112012890)|
|Flink读ClickHouse||
|[Flink写ClickHouse](写入ClickHouse)|[csv-＞Flink SQL-＞Clickhouse(纯DDL形式)](https://yuchi.blog.csdn.net/article/details/112688042)|
|[Flink SQL Client读寫Hive](https://yuchi.blog.csdn.net/article/details/111462921)|完成|
|[Flink SQL Client读取kafka+传入Mysql](./flink-sql-submit/src/main/resources/q1.sql)|[Kafka2.5-＞Flink1.12-＞Mysql8(Jark实验改为DDL形式)](https://yuchi.blog.csdn.net/article/details/111645917)
|[Kafka与Mysql的Join(DDL形式)](KafkaMysqlJoinDDL.sql)|[Flink进行Kafka事实表与Mysql维度表Join(纯DDL/SQL方式)](https://yuchi.blog.csdn.net/article/details/111867503)
|[Flink SQL解析复杂Json](json_parse.sql)|[Flink SQL解析复杂Join](https://yuchi.blog.csdn.net/article/details/111873947)
|[事实表(Kafka)与维度表(Hbase)进行Join](Correlate_hbase.sql)|[Flink SQL Client进行Kafka事实表与Hbase维度表Join(纯DDL/SQL方式)](https://yuchi.blog.csdn.net/article/details/111875346)
|[Hive的GenericUDAF用法](Java/src/main/java/FieldLength.java)|[GenericUDAF使用流程记载](https://yuchi.blog.csdn.net/article/details/112068144)
|[Hive的GenericUDTF用法](Java/src/main/java/ArrToMapUDTF.java)|[GenericUDTF使用流程记载](https://yuchi.blog.csdn.net/article/details/112071373)
|[Flink SQL Client CDC流程](cdc.sql)|[Flink SQL Client实现CDC实验](https://yuchi.blog.csdn.net/article/details/112008780)

|API名称	|flink-jdbc<br>(Flink≤1.10.1版本)	|flink-connector-jdbc<br>(Flink≥1.11.0版本)
|---|---|---|
|DataStream	|不支持	|支持|
|Table API (Legecy)	|支持	|不支持
|Table API (DDL)	|不支持	|不支持



对于Mysql,事先按照下面准备好:<br>
[mysql练习用的数据集下载(转载+自己补充步骤)](https://yuchi.blog.csdn.net/article/details/79439387)




