import ProcessFunctionScalaV2.DataJast
import org.apache.flink.api.common.functions.AggregateFunction //计算keyby后，每个Window中的数据总和
class CountAggregate extends AggregateFunction[(String, Long), DataJast, DataJast] {

  override def createAccumulator(): DataJast = {
    println("初始化")
    DataJast(null, 0)
  }

  override def add(value: (String, Long), accumulator: DataJast): DataJast = {
    if (accumulator.key == null) {
      printf("第一次加载,key:%s,value:%d\n", value._1, value._2)
      DataJast(value._1, value._2)
    } else {
      printf("数据累加,key:%s,value:%d\n", value._1, accumulator.count + value._2)
      DataJast(value._1, accumulator.count + value._2)
    }
  }

  override def getResult(accumulator: DataJast): DataJast = {
    println("返回结果：" + accumulator)
    accumulator
  }

  override def merge(a: DataJast, b: DataJast): DataJast = {
    DataJast(a.key, a.count + b.count)
  }
}
