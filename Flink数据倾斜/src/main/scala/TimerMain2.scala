
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object TimerMain2
{

  def main(args: Array[String]): Unit =
  {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env
      .addSource(new MySourceTuple2)
      .keyBy(_._1)
      .process(KeyedProcessFunction2)
      .print("处理结果：")
    env.execute()
  }

}