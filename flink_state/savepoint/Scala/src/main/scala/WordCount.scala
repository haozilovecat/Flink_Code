import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._

object WordCount {
  def main(args: Array[String]): Unit =
  {
    val streamEnv = StreamExecutionEnvironment.getExecutionEnvironment
    streamEnv.setParallelism(1)

    //读取数据到DataStream
    val stream = streamEnv.socketTextStream("Desktop", 9999).uid("mySource-001")

    stream.flatMap(_.split(" "))
      .uid("flapMap-001")
      .map((_, 1))
      .uid("map=001")
      .keyBy(0)
      .sum(1)
      .uid("sum-001")
      .print()

    //启动流计算
    streamEnv.execute("wc")
  }
}