public class Order {
    //finishTime: Long, memberId: Long, productId: Long, sale: Double
    public long finishTime;
    public long memberId;
    public long productId;
    public double sale;

    public Order() {
    }

    public Order(Long finishTime, Long memberId, Long productId, Double sale) {
        this.finishTime = finishTime;
        this.memberId = memberId;
        this.productId = productId;
        this.sale = sale;
    }



    public String toString()
    {
        return "memberId="+memberId+","+"productId="+productId+","+"sale="+sale;
    }




}