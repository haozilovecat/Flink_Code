import org.apache.flink.streaming.api.functions.source.RichSourceFunction;


public class RichSourceFunction1 extends RichSourceFunction<String> {
    private boolean isCanaled = false;

    @Override
    public void run(SourceContext<String> ctx) throws Exception {
        while (!isCanaled) {
            ctx.collect("hadoop flink spark");
            Thread.sleep(1000);
        }
    }

    @Override
    public void cancel() {
        isCanaled = true;
    }
}
