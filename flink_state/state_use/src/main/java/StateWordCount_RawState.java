import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.StateBackend;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.nio.file.Path;

public class StateWordCount_RawState
{

    public static void main(String[] args) throws Exception
    {

        final ParameterTool parameters = ParameterTool.fromArgs(args);
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().setGlobalJobParameters(parameters);

        // Checkpoint
        env.enableCheckpointing(5000, CheckpointingMode.EXACTLY_ONCE);

        // StateBackend
//        StateBackend stateBackend = new FsStateBackend(new Path("hdfs://Desktop:9000/user/hdfs/flink/checkpoints"));
        StateBackend stateBackend = new FsStateBackend("hdfs://Desktop:9000/user/hdfs/flink/checkpoints");

        env.setStateBackend(stateBackend);

        env
                .addSource(new SourceFromFile())
                .setParallelism(1)
                .flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
                    @Override
                    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
                        String[] arr = value.split(",");
                        for (String item : arr) {
                            out.collect(new Tuple2<>(item, 1));
                        }
                    }
                })
                .keyBy(0)
                .process(new WordCountProcess())
                .print();

        env.execute("StateWordCount");
    }
}
