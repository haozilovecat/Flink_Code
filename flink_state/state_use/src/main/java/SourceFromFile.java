import java.io.FileReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import java.io.BufferedReader;
import java.util.concurrent.TimeUnit;

public class SourceFromFile extends RichSourceFunction<String> {
    private volatile Boolean isRunning = true;

    @Override
    public void run(SourceContext ctx) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("hdfs://Desktop:9000/test.txt"));
        while (isRunning) {
            String line = bufferedReader.readLine();
            if (StringUtils.isBlank(line)) {//如果该行是空行，直接跳过
                continue;
            }
            ctx.collect(line);
//            TimeUnit.SECONDS.sleep(60);
            TimeUnit.SECONDS.sleep(3);
        }
    }

    @Override
    public void cancel() {
        isRunning = false;
    }
}