use db1;
CREATE external TABLE fs_table (
  user_id STRING,
  order_amount DOUBLE) partitioned by (dt string,h string,m string) stored as ORC TBLPROPERTIES (
  'partition.time-extractor.timestamp-pattern'='$dt $h:$m:00',
  'sink.partition-commit.delay'='0s',
  'sink.partition-commit.trigger'='partition-time',
  'sink.partition-commit.policy.kind'='metastore');

然后运行StreamingWriteHive.java
