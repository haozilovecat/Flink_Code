public  class WordCount
{
    public String word;
    public long frequency;//这里不能用count表示，属于flink sql保留关键词...参考：https://ci.apache.org/projects/flink/flink-docs-release-1.10/dev/table/sql/index.html#reserved-keywords

    //这个无参构造方法必须要有，要不会报错...参考：https://ci.apache.org/projects/flink/flink-docs-release-1.10/zh/dev/api_concepts.html#pojo
    //org.apache.flink.table.api.ValidationException: Too many fields referenced from an atomic type.
    public WordCount()
    {
    }

    public WordCount(String word, long frequency)
    {
        this.word = word;
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return word + ", " + frequency;
    }
}