
import lombok.Data;
import java.sql.Timestamp;

@Data
public class StudentInfo{

    private String name;
    private String sex;
    private String course;
    private Float score;
    private Long timestamp;  //sql 关键字，sql中不可以直接使用
    private Timestamp sysDate;
}