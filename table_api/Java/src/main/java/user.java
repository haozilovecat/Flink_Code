
// *************************************************************************
//     USER DATA TYPES
// *************************************************************************

/**
 * Simple POJO.
 */
public class user
{
    public int id;
    public String name;
    public int kindid;

    public user()
    {
    }

    public user(int id, String name, int kindid)
    {
        this.id=id;
        this.name = name;
        this.kindid = kindid;
    }

    @Override
    public String toString() {
        return "user{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", kindid=" + kindid +
                '}';
    }
}