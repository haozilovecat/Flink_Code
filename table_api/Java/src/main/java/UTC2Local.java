
import org.apache.flink.table.functions.ScalarFunction;
import java.sql.Timestamp;

public class UTC2Local extends ScalarFunction {

    public Timestamp eval(Timestamp s) {
        long timestamp = s.getTime() + 28800000; //flink默认的是UTC时间，我们的时区是东八区，时间戳需要增加八个小时
        return new Timestamp(timestamp);
    }

}