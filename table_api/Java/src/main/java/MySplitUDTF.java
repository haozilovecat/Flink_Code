import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.table.functions.FunctionContext;
import org.apache.flink.table.functions.TableFunction;

public class MySplitUDTF extends TableFunction<Tuple3<String,String,String>>
{

    // 可选，open方法可不编写。如果编写，则需要添加声明'import org.apache.flink.table.functions.FunctionContext;'。
    @Override
    public void open(FunctionContext context) {
        // ... ...
    }

    public void eval(String str) {
        String[] split = str.split("#");
//        for (String s : split)
//        {
//            collect(s);
//        }

        Tuple3<String,String,String> tuple3=Tuple3.of(split[0],split[1],split[2]);
        collect(tuple3);

    }

    // 可选，close方法可不编写。
    @Override
    public void close() {
        // ... ...
    }
}