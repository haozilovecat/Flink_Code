import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.table.functions.ScalarFunction;
import org.apache.flink.types.Row;

public class MyMapFunction extends ScalarFunction
{
    public Row eval(String a)
    {
        return Row.of(a, "pre-" + a);
    }

    @Override
    public TypeInformation<?> getResultType(Class<?>[] signature)
    {
        return Types.ROW(Types.STRING, Types.STRING);

    }
}