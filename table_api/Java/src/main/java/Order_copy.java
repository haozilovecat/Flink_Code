
// *************************************************************************
//     USER DATA TYPES
// *************************************************************************

/**
 * Simple POJO.
 */
public class Order_copy
{
    public Long userid;
    public String fruit;
    public int number;

    public Order_copy()
    {
    }

    public Order_copy(Long userid, String fruit, int number)
    {
        this.userid = userid;
        this.fruit = fruit;
        this.number = number;
    }

    @Override
    public String toString() {
        return "Order_copy{" +
                "user=" + userid +
                ", product='" + fruit + '\'' +
                ", number=" + number +
                '}';
    }
}