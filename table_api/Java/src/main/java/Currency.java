import java.sql.Timestamp;

public class Currency
{
    public String o_currency;
    public Long o_rate;
    public Timestamp o_proctime;


    public Currency()
    {
    }

    public Currency(String o_currency,  Long o_rate, Timestamp o_proctime)
    {
        this.o_currency = o_currency;
        this.o_rate = o_rate;
        this.o_proctime = o_proctime;
    }

    @Override
    public String toString()
    {
        return "Currency{" +
                "o_currency=" + o_currency +
                ", o_rate='" + o_rate + '\'' +
                ", o_proctime=" + o_proctime +
                '}';
    }
}