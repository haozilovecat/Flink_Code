
// *************************************************************************
//     USER DATA TYPES
// *************************************************************************

/*
 * Simple POJO.
 */


import java.sql.Timestamp;
import org.apache.flink.streaming.api.windowing.time.Time;
/**
 * Simple POJO.
 */
public class OrderStream
{
    public int id;
    public Long user;
    public String product;
    public int amount;
    public Long rowtime;

    public OrderStream()
    {
    }

    public OrderStream(int id,Long user,String product,int amount,Long rowtime)
    {
        this.id=id;
        this.user = user;
        this.product = product;
        this.amount = amount;
        this.rowtime = rowtime;
    }

    @Override
    public String toString()
    {
        return "Order{" +
                "id="+id+
                ", user=" + user +
                ", product='" + product + '\'' +
                ", amount=" + amount +
                ", ts=" + rowtime +
                '}';
    }
}
