import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.table.functions.TableFunction;

public class Split extends TableFunction<Tuple2<String, Integer>>
{

    private String separator = " ";
    public Split(String separator)
    {
        this.separator = separator;
    }
    public void eval(String str)
    {
        for (String s : str.split(separator))
        {
            // use collect(...) to emit a row
            collect(new Tuple2<String, Integer>(s, s.length()));
        }
    }
}

//这里之所以是TableFunction<Tuple2<String, Integer>>
//这种一般是为了处理带有时间戳的数据