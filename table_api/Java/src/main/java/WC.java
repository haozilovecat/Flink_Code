// *************************************************************************
//     USER DATA TYPES
// *************************************************************************

/**
 * Simple POJO containing a word and its respective count.
 */
public  class WC {
    public String word;
    public long frequency;

    // public constructor to make it a Flink POJO
    public WC() {}

    public WC(String word, long frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "WC " + word + " " + frequency;
    }
}