use test;

CREATE TABLE Customers (
  customerid char(5) NOT NULL,
  city varchar (10) NOT NULL
);

insert into Customers values('C001','Beijing');
insert into Customers values('C002','Beijing');
insert into Customers values('C003','Beijing');
insert into Customers values('C004','HangZhou');

CREATE TABLE Orders(
  orderid char(5) NOT NULL,
  customerid char(5) NULL
);

insert into Orders values('O001','C001');
insert into Orders values('O002','C001');
insert into Orders values('O003','C003');
insert into Orders values('O004','C001');


SELECT c.customerid, c.city, o.orderid FROM Customers c JOIN Orders o ON o.customerid = c.customerid;
SELECT c.customerid, c.city FROM Customers c WHERE c.customerid IN (SELECT o.customerid, o.orderid FROM Orders o WHERE o.customerid = c.customerid);
