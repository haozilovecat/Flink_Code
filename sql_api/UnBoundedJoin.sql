-- 该实验总共t1,t2,t3,t4四个表
-- t1,t2进行unBoundedJoin操作
-- t3,t4进行interval Join操作

CREATE TABLE t1 (
    user_id BIGINT,
    order_id BIGINT,
    ts BIGINT
) WITH (
    'connector.type' = 'kafka',  -- 指定连接类型是kafka
    'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
    'connector.topic' = 'unBoundedJoin01_t1', -- 之前创建的topic 
    'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
    'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
    'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
    'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
    'format.type' = 'csv'  -- csv格式，和topic中的消息格式保持一致
);


CREATE TABLE t2 (
    order_id BIGINT,
    item_id BIGINT,
    ts BIGINT
) WITH (
    'connector.type' = 'kafka',  -- 指定连接类型是kafka
    'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
    'connector.topic' = 'unBoundedJoin01_t2', -- 之前创建的topic 
    'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
    'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
    'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
    'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
    'format.type' = 'csv'  -- csv格式，和topic中的消息格式保持一致
);


CREATE TABLE t3 (
    user_id BIGINT,
    order_id BIGINT,
    ts BIGINT,
    r_t AS TO_TIMESTAMP(FROM_UNIXTIME(ts,'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd HH:mm:ss'),-- 计算列，因为ts是bigint，没法作为水印，所以用UDF转成TimeStamp
    WATERMARK FOR r_t AS r_t - INTERVAL '5' SECOND -- 指定水印生成方式
) WITH (
    'connector.type' = 'kafka',  -- 指定连接类型是kafka
    'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
    'connector.topic' = 'timeIntervalJoin_01', -- 之前创建的topic 
    'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
    'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
    'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
    'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
    'format.type' = 'csv'  -- csv格式，和topic中的消息格式保持一致
);


CREATE TABLE t4 (
    order_id BIGINT,
    item_id BIGINT,
    ts BIGINT,
    r_t AS TO_TIMESTAMP(FROM_UNIXTIME(ts,'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd HH:mm:ss'),-- 计算列，因为ts是bigint，没法作为水印，所以用UDF转成TimeStamp
    WATERMARK FOR r_t AS r_t - INTERVAL '5' SECOND -- 指定水印生成方式
) WITH (
    'connector.type' = 'kafka',  -- 指定连接类型是kafka
    'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
    'connector.topic' = 'timeIntervalJoin_02', -- 之前创建的topic 
    'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
    'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
    'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
    'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
    'format.type' = 'csv'  -- csv格式，和topic中的消息格式保持一致
);

---双流Join 之Inner Join-数据集
---t1 543462,1001,1511658000
---t2 1001,4238,1511658001
---双流Join 之Inner Join-SQL语句
select a.*,b.* from t1 a inner join t2 b on a.order_id = b.order_id;
-- 需要执行12s






---双流Join 之 Left Join-数据集
---t1 223813,2042400,1511658002
---t2 2042400,4104826,1511658001
---双流Join 之 Left Join-SQL语句
select a.*,b.* from t1 a left join t2 b on a.order_id = b.order_id;
-- 需要执行12s