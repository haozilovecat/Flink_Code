import com.alibaba.fastjson.JSON;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Json_String_List_User {


    public static void main(String[] args) throws FileNotFoundException, IOException {
        //---------------------------json字符串 -> List<User>对象------------------------
        /*
         * 字符串：[{"password":"123123","username":"zhangsan"
         * },{"password":"321321","username":"lisi"}]
         */
        String jsonStr2 = "[{'password':'123123','username':'zhangsan'},{'password':'321321','username':'lisi'}]";
        List<User> users = JSON.parseArray(jsonStr2, User.class);
        System.out.println("json字符串转List<Object>对象:" + users.toString());


    }

}
