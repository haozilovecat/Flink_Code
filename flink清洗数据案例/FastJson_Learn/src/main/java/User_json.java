import com.alibaba.fastjson.JSON;
import java.io.FileNotFoundException;
import java.io.IOException;

public class User_json
{
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
//---------------------------User->转json字符串---------------------------

        User user = new User("dmego", "123456");
        String UserJson = JSON.toJSONString(user);//转化为一个json格式的字符串
        System.out.println("简单java类转json字符串:" + UserJson);
    }

}
