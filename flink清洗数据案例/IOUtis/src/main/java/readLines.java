import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class readLines {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        try (FileInputStream fin = new FileInputStream("test3.txt")) {
            List ls = IOUtils.readLines(fin, "utf-8");
            for (int i = 0; i < ls.size(); i++) {
                System.out.println(ls.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
