import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

public class IOUtis_use {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("当前路径是:\n"+System.getProperty("user.dir"));


//读数据
        String str=IOUtils.toString(new FileInputStream("read.txt"));
        System.out.println(str);

////写数据
        IOUtils.write(str, new FileOutputStream("write.txt"));

//复制文件中的内容
        IOUtils.copy(new FileInputStream("read.txt"), new FileOutputStream("copy.txt"));


    }
}