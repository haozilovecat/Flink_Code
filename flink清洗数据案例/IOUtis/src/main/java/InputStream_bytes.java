import org.apache.commons.io.IOUtils;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class InputStream_bytes
{

    public static void main(String[] args) throws IOException
    {
        try(FileInputStream fin=new FileInputStream("test2.txt")){
            byte[] buf= new byte[100];
            int len= IOUtils.read(fin,buf);
            System.out.println("The Length of Input Stream:"+len);
        }catch (IOException e){
            e.printStackTrace();
        }
    }


}
