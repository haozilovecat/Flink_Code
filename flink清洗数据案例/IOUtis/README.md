
|功能|备注|
|---|---|
|[读数据](src/main/java/IOUtis_use.java)|IOUtils|
|[写数据](src/main/java/IOUtis_use.java)|IOUtils|
|[1个文件拷贝到另外1个文件](src/main/java/IOUtis_use.java)|IOUtils|
|[1个文件拷贝到另外1个文件](src/main/java/FileUtils_use.java)|FileUtils|
|[String->InputStream->OutputStream->text](src/main/java/InputStream_OutputStream.java)|IOUtils
|[Text->FileReader(打印到控制台)]((src/main/java/text_FileReader.java)|IOUtils|
|[Text->InputStream->byte[]](src/main/java/InputStream_bytes.java)|IOUtils
|[readLines用法](src/main/java/readLines.java)|IOUtils
|[writeLines用法](src/main/java/writeLines.java)|IOUtils
|[LineIterator用法](src/main/java/LineIterator.java)|IOUtils|
|[IOUtils.write用法](src/main/java/IOUtils_write.java)|IOUtils


参考文献:
[IOUtils](https://blog.csdn.net/qidasheng2012/article/details/87922190)
[IOUtils使用介绍](https://blog.csdn.net/l2580258/article/details/89227761)
[Apache IOUtils的使用](https://www.cnblogs.com/bfcs/p/10426659.html)