import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.util.Collector;
public class PointWeighter2
        implements FlatJoinFunction<Rating, Tuple2<String, Double>, Tuple2<String, Double>> {
    @Override
    public void join(Rating rating, Tuple2<String, Double> weight,
                     Collector<Tuple2<String, Double>> out) {
        if (weight.f1 > 0.1) {
            out.collect(new Tuple2<String, Double>(rating.name, rating.points * weight.f1));
        }
    }
}