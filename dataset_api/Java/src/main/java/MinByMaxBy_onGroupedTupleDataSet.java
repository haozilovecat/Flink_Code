import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;

import org.apache.flink.api.java.ExecutionEnvironment;


    public class MinByMaxBy_onGroupedTupleDataSet{
        public static void main(String[] args) throws Exception {


        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");
        DataSet<Tuple3<Integer, String,Double>> input =env.readCsvFile("file://"+rootPath+"/"+"aggregate.csv").types(Integer.class, String.class,Double.class);
        input.print();
        System.out.print("----------------------------------------------------\n");

        DataSet<Tuple3<Integer, String, Double>> output = input
                .groupBy(1)   // group DataSet on second field
                .minBy(0, 2); // select tuple with minimum values for first and third field.
//对于第0和第2个key选择最小值作为最终值

            output.print();

}

}
