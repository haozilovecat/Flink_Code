import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

public class GroupReduceonfullDataSet
{


        public static void main(String[] args) throws Exception
        {

            ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
            String rootPath = System.getProperty("user.dir");
            DataSet<Integer> input = env.readTextFile("file://"+rootPath+"/"+"number.csv").map(new MapFunction<String, Integer>() {
                @Override
                public Integer map(String s) throws Exception
                {
                    return Integer.valueOf(s);
                }
            });
            input.print();

// apply a (preferably combinable) GroupReduceFunction to a DataSet
            DataSet<Integer> output = input.reduceGroup(new MyGroupReducer());
            System.out.println("-----------------------输出最终结果-----------------------");
            output.print();


    }

}
