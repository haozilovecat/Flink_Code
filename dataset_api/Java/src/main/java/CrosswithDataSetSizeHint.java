import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.CrossOperator;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import org.apache.flink.api.java.DataSet;


public class CrosswithDataSetSizeHint {



    public static void main(String[] args) throws Exception {



        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");
        DataSet<Tuple2<Integer,String>> input1 = env.readCsvFile("file://"+rootPath+"/"+"group.csv").types(Integer.class,String.class);
        DataSet<Tuple2<Integer,String>> input2 = env.readCsvFile("file://"+rootPath+"/"+"group1.csv").types(Integer.class,String.class);

        DataSet<Tuple2<Tuple2<Integer, String>, Tuple2<Integer, String>>> udfResult = input1.crossWithTiny(input2);// hint that the second DataSet is very small
        // apply any Cross function (or projection)
//                        .with(new MyCrosser());


        System.out.println("-------------------------------------输出udfResult----------------------------------------------------");

        udfResult.print();




        DataSet<Tuple3<Integer, Integer, String>>
                projectResult =
                // hint that the second DataSet is very large
                input1.crossWithHuge(input2)
                        // apply a projection (or any Cross function)
                        .projectFirst(0,1).projectSecond(1);


        System.out.println("-------------------------------------输出projectResult----------------------------------------------------");
        projectResult.print();
        
        
//        代码的目的就是求笛卡尔积(求全排列)
//        上述代码中，两个不同的输出，主要是输出顺序不同


    }
}
