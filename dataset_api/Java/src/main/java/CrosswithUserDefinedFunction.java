import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.ExecutionEnvironment;


public class CrosswithUserDefinedFunction {

    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();


        Coord coord1=new Coord(1,35,67);
        Coord coord2=new Coord(5,98,27);
        DataSet<Coord> coords1 =env.fromElements(coord1);
        DataSet<Coord> coords2 =env.fromElements(coord2);

        DataSet<Tuple3<Integer, Integer, Double>>
                distances =
                coords1.cross(coords2)
                        // apply CrossFunction
                        .with(new EuclideanDistComputer());

        distances.print();


    }
}
