import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
class MapPartition   {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<String> textLines=senv.fromElements("India", "USA","China","America");
        DataSet<Long> counts = textLines.mapPartition(new PartitionCounter());

        counts.print();

    }

}




