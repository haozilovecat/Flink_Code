import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.common.functions.CrossFunction;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

public class EuclideanDistComputer
        implements CrossFunction<Coord, Coord, Tuple3<Integer, Integer, Double>> {

    @Override
    public Tuple3<Integer, Integer, Double> cross(Coord c1, Coord c2) {
        // compute Euclidean distance of coordinates
        double dist = sqrt(pow(c1.x - c2.x, 2) + pow(c1.y - c2.y, 2));
        return new Tuple3<Integer, Integer, Double>(c1.id, c2.id, dist);
    }
}