import org.apache.flink.api.common.operators.base.JoinOperatorBase.JoinHint;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;


public class JoinAlgorithmHints
{


    public static void main(String[] args) throws Exception
    {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");

        DataSet<Tuple2<Integer, String>> input1 = env.readCsvFile("file://" + rootPath + "/" + "group.csv").types(Integer.class, String.class);
        DataSet<Tuple2<Integer, String>> input2 = env.readCsvFile("file://" + rootPath + "/" + "group2.csv").types(Integer.class, String.class);

        DataSet< Tuple2<Tuple2<Integer, String>,Tuple2<Integer, String>>> result =input1.join(input2, JoinHint.BROADCAST_HASH_FIRST)
                .where(0).equalTo(0);

//        两个dataset根据第0列数据进行join操作
        result.print();


    }

}