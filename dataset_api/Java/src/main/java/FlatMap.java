import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;





class FlatMap {
    public static void main(String[] args) throws Exception {

        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<String> textLines=senv.fromElements("India", "USA");
        DataSet<String> words = textLines.flatMap(new Tokenizer());

//        System.out.println("---------------------------------");
//        String rootPath = System.getProperty("user.dir");
//        System.out.println(rootPath);


        words.print();

    }

}
