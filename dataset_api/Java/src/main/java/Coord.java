public class Coord {
    public int id;
    public int x;
    public int y;

    public  Coord(int id,int x,int y){
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public Coord(){}
    @Override
    public String toString()
    {
        return id+","+ x+","+y;
    }



}