import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class IntAdder implements MapFunction<Tuple2<Integer, Integer>, Integer> {
    @Override
    public Integer map(Tuple2<Integer, Integer> in) {
        return in.f0 + in.f1;
    }
}