import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
class Filter  {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<Integer> intNumbers=senv.fromElements(1,2,3,4,5,6);
        DataSet<Integer> naturalNumbers = intNumbers.filter(new NaturalNumberFilter());
        naturalNumbers.print();

    }

}




