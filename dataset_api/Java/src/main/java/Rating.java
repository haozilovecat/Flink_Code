public class Rating {
    public String name;
    public String category;
    public int points;

    public  Rating(String name,String category,int points){
        this.name = name;
        this.category = category;
        this.points=points;
    }

    public Rating(){}
    @Override
    public String toString()
    {
        return name+","+category+","+points;
    }





}