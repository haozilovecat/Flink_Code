import org.apache.flink.api.common.functions.GroupCombineFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;




public class MyGroupReducer  implements  GroupReduceFunction<Integer,Integer>
{

    //--------------------------覆盖reduce算子---------------------------
    @Override
    public void reduce(Iterable<Integer> in, Collector<Integer> out)
    {

//        String key = null;
        int sum = 0;

        for (Integer curr : in)
        {
//            key = curr.f0;
            sum += curr;
        }
        // concat key and sum and emit
        out.collect(sum);
    }

}
