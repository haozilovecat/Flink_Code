import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.DataSet;





public class ReduceonDataSetGroupedbyFieldPositionKeys {


    public static void main(String[] args) throws Exception {

        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");
        System.out.println("输出路径="+rootPath);


        DataSet<Tuple3<String, Integer, Double>> tuples = senv.readCsvFile("file://"+rootPath+"/"+"reduce.csv").types(String.class, Integer.class, Double.class);
        System.out.println(tuples.collect());
//        [("study",6,4.56), ("study",4,7.56), ("study",6,4.56), ("study",4,7.56)]

        DataSet<Tuple3<String, Integer, Double>> reducedTuples =tuples.groupBy(0, 1).reduce(new MyTupleReducer());
//      这里groupby(0,1)的意思是先按照第一个key进行归类，再这个基础上，再按照第一个value进行归类。


         reducedTuples.print();
    }

}

