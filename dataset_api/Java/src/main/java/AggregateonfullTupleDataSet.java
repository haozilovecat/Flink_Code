import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import static org.apache.flink.api.java.aggregation.Aggregations.MIN;
import static org.apache.flink.api.java.aggregation.Aggregations.SUM;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;

public class AggregateonfullTupleDataSet {

    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");

        DataSet<Tuple2<Integer, Double>> input = env.readCsvFile("file://" + rootPath + "/" + "tupledata.csv").types(Integer.class, Double.class);


        input.print();
        System.out.println("--------------------------------------");
        DataSet<Tuple2<Integer, Double>> output = input
                .aggregate(SUM, 0)    // compute sum of the first field
                .and(MIN, 1);    // compute minimum of the second field


        output.print();
//        第0个key求和,
//        第1个key求最小值
    }

}
