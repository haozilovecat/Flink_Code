import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;

public interface JoinwithFlatJoinFunction
{



    public static void main(String[] args) throws Exception
    {

        Rating rating1=new Rating("yuchi","run",50);
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<Rating> ratings = env.fromElements(rating1);
        String rootPath = System.getProperty("user.dir");
        DataSet<Tuple2<String, Double>> weights = env.readCsvFile("file://" + rootPath + "/" + "flatjoin.csv").types(String.class, Double.class);
        DataSet<Tuple2<String, Double>>weightedRatings =ratings.join(weights)
                .where("category")
                // key of the second input
                .equalTo("f0")
                // applying the JoinFunction on joining pairs
                .with(new PointWeighter2());

        weightedRatings.print();

        }
}



