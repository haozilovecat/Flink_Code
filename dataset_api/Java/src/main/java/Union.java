import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;


public class Union {

    public static void main(String[] args) throws Exception {

//        每行数据视为最小单位,汇总,不去重

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");

    DataSet<Tuple2<String, Integer>> vals1 = env.readCsvFile("file://"+rootPath+"/"+"cog1.csv").types(String.class, Integer.class);
    DataSet<Tuple2<String, Integer>> vals2 = env.readCsvFile("file://"+rootPath+"/"+"cog2.csv").types(String.class, Integer.class);
    DataSet<Tuple2<String, Integer>> vals3 = env.readCsvFile("file://"+rootPath+"/"+"cog3.csv").types(String.class, Integer.class);

    DataSet<Tuple2<String, Integer>> unioned = vals1.union(vals2).union(vals3);
    unioned.print();


}

}
