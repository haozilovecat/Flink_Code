import org.apache.flink.api.common.functions.ReduceFunction;

import org.apache.flink.api.java.tuple.Tuple3;



public  class MyTupleReducer implements ReduceFunction<Tuple3<String,Integer,Double>> {

    @Override
    public Tuple3<String, Integer, Double> reduce(Tuple3<String, Integer, Double> t3,
                                                  Tuple3<String, Integer, Double> t1) throws Exception {
        return new Tuple3<>(t3.f0,t3.f1+t1.f1,t3.f2+t1.f2);
    }
}