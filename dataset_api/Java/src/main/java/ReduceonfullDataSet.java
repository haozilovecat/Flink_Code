import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;

public class ReduceonfullDataSet {


    public static void main(String[] args) throws Exception {




        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");
//        DataSet<Tuple3<Integer, String,Double>> input =env.readCsvFile("file://"+rootPath+"/"+"aggregate.csv").types(Integer.class, String.class,Double.class);


        DataSet<Integer> intNumbers = env.fromElements( 1 , 2 , 3 );


        DataSet<Integer> sum = intNumbers.reduce(new IntSummer());

        sum.print();

    }


}






