import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

public class Tokenizer implements FlatMapFunction<String, String> {
    @Override
    public void flatMap(String value, Collector<String> out) {
        for (String token : value.split("\\W")) {
            out.collect(token);
        }
    }
}