import org.apache.flink.api.common.functions.ReduceFunction;
public class IntSummer implements ReduceFunction<Integer> {
    @Override
    public Integer reduce(Integer num1, Integer num2) {
        return num1 + num2;
    }
}