import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;

public class Distinct {

    public static void main(String[] args) throws Exception {



        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = System.getProperty("user.dir");
        DataSet<Tuple2<Integer, Double>> input = env.readCsvFile("file://"+rootPath+"/"+"tupledata.csv").types(Integer.class,Double.class);

        input.print();
        System.out.println("---------------------------------------------------------");
        DataSet<Tuple2<Integer, Double>> output = input.distinct();//整行数据为单位,进行去重
        output.print();


    }
}
