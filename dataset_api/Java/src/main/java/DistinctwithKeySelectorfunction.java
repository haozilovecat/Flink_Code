import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;


public class DistinctwithKeySelectorfunction {
    public static void main(String[] args) throws Exception {

    ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

    DataSet<Integer> input = env.fromElements(-2,2,3);


    input.print();
    System.out.println("-----------------------------------------------------");
    DataSet<Integer> output = input.distinct(new AbsSelector());


    System.out.println("--------------------------Final Result-----------------------------");
    output.print();

}


}
