public class Order2
{
    public int id;
    public String name;
    public int price;

    public Order2()
    {
    }

    public Order2(int id,String name,int price)
    {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "Order2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}