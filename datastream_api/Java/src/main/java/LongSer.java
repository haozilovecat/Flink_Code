import java.io.Serializable;

public class LongSer implements Serializable {

    private static final long serialVersionUID = -6849794470754667710L;
   private long value;

    public LongSer(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
