public  class Transcript{

    public String id;
    public String name;
    public String subject;

    public int score;

    public long time;

    public Transcript(String id, String name, String subject, int score, long time) {

        this.id = id;

        this.name = name;

        this.subject = subject;

        this.score = score;

        this.time = time;

    }

    public String getId() {

        return id;

    }

    public void setId(String id) {

        this.id = id;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public String getSubject() {

        return subject;

    }

    public void setSubject(String subject) {

        this.subject = subject;

    }

    public int getScore() {

        return score;

    }

    public void setScore(int score) {

        this.score = score;

    }

    public long getTime() {

        return time;

    }

    public void setTime(long time) {

        this.time = time;

    }

}