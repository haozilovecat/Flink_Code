import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Project
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<Tuple3<Integer, Double, String>> in =env.fromElements(Tuple3.of(6,7.12,"hello"));
        DataStream<Tuple2<String, Integer>> out = in.project(2,0);//根据下标进行映射
        out.print();
        env.execute();

    }
}
