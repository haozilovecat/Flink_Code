import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;

public class AnatomyofaFlinkProgram
{

    public static void main(String[] args) throws Exception
    {


        StreamExecutionEnvironment senv = StreamExecutionEnvironment.getExecutionEnvironment();

        String rootPath = System.getProperty("user.dir");


        System.out.println("rootPath="+rootPath);

        DataStream<String> input = senv.readTextFile("file://" + rootPath + "/" + "map.csv");


        System.out.println("-------------------------输出input------------------------------------");
        DataStream<Integer> parsed = input.map(new MapFunction<String, Integer>()
        {
            @Override
            public Integer map(String value)
            {

                return Integer.valueOf(value.split(",")[0]);
            }
        });


        parsed.print();

        senv.execute();

    }


}
