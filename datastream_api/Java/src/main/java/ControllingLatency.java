import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.environment.LocalStreamEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class ControllingLatency
{


    public static void main(String[] args) throws Exception
    {

        LocalStreamEnvironment env = StreamExecutionEnvironment.createLocalEnvironment();
        env.setBufferTimeout(5);

        env.generateSequence(1,10).map(new MapFunction<Long, Object>() {
            @Override
            public Object map(Long aLong) throws Exception {
                return aLong*2;
            }
        }).setBufferTimeout(5).print();

    }

//    数据读入缓冲区以后，等待多久才会输出。


}