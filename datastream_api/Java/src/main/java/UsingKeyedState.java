import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class UsingKeyedState
{


    public static void main(String[] args) throws Exception
    {
        //获取Flink的运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.fromElements(
                Tuple2.of(1L, 3L),
                Tuple2.of(1L, 5L), //求均值

                Tuple2.of(1L, 7L),
                Tuple2.of(1L, 4L), //求均值

                Tuple2.of(1L, 2L))
                .keyBy(0)
                .flatMap(new CountWindowAverage())
                .print();


//        这里的CountWindowAverage的意思是每２个求均值
        env.execute("StafulOperator");
        System.out.println("******代码运行结束*****");
    }






}

//    代码来自:
//    https://juejin.im/post/6844903721395027975