import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestHelloUdf{

    private HelloUdf helloUdf;

    @Before
    public void setUp(){
        helloUdf = new HelloUdf();
    }

    @After
    public void tearDown(){
        helloUdf = null;
    }

    @Test
    public void testUdf()
    {
        String input = "John";
        String result = helloUdf.evaluate(input);
        System.out.println(result);
    }
    }